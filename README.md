# Building and running

To build the code use:

```bash
mkdir build
cmake -B build/ -S .
cd build
make -j4
```

## Requirements

- `geant4` version 11+
- CERN ROOT
- For the helper script: PyROOT, numpy, matplotlib

To run the code

```bash
./build/giantdipole # With GUI
./build/giantdipole macro.mac # Without GUI
```

# Macro file

All settings are located in `macro.mac`. The code there should be self-explanatory.

# Random seed

The random seed is taken from the `/run_params/setJobID [job id string]`. Be sure that `[job id string]` contains a unique number for each job!
