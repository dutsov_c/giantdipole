#include "PrimaryGeneratorAction.hh"
#include "G4GeneralParticleSource.hh" // Ensure GPS is included
#include "G4ParticleTable.hh"
#include "G4SystemOfUnits.hh"

PrimaryGeneratorAction::PrimaryGeneratorAction() : particleGun(new G4GeneralParticleSource()) {
    // No need to set particle definition here, configure via macro commands
}

PrimaryGeneratorAction::~PrimaryGeneratorAction() {
    delete particleGun;
}

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent) {
    particleGun->GeneratePrimaryVertex(anEvent);
}

