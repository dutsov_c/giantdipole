#include "NeutronSD.hh"

#include "G4Step.hh"
#include "G4Neutron.hh"
#include "G4SystemOfUnits.hh"
#include "G4TouchableHistory.hh"
#include "G4AnalysisManager.hh"

NeutronSD::NeutronSD(const G4String& name) : G4VSensitiveDetector(name) {}

NeutronSD::~NeutronSD() {}

G4bool NeutronSD::ProcessHits(G4Step* aStep, G4TouchableHistory*) {
    G4Track* track = aStep->GetTrack();
    G4ParticleDefinition* particle = track->GetDefinition();
    /* if (particle != G4Neutron::Definition()) return false; */

    // Check if the neutron is exiting the boundary
    G4StepPoint* preStepPoint = aStep->GetPreStepPoint();
    G4StepPoint* postStepPoint = aStep->GetPostStepPoint();
    G4LogicalVolume* preStepVolume = preStepPoint->GetTouchableHandle()->GetVolume()->GetLogicalVolume();
    G4LogicalVolume* postStepVolume = postStepPoint->GetTouchableHandle()->GetVolume()->GetLogicalVolume();

    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

    // Only interested in boundary exit: preStepVolume is this detector, postStepVolume is not
    if(postStepPoint->GetStepStatus() == fGeomBoundary && track->GetTrackID() != 1) {
        G4double edep = track->GetKineticEnergy();
        G4ThreeVector pos = postStepPoint->GetPosition();
        G4ThreeVector mom = postStepPoint->GetMomentum();
        /* std::cout << "Neutron exiting! Energy: " << edep/keV << " keV, Momentum: (" */
        /*           << mom.x()/MeV << ", " << mom.y()/MeV << ", " << mom.z()/MeV */
        /*           << ") MeV, Position: (" << pos.x()/cm << ", " << pos.y()/cm << ", " << pos.z()/cm << ") cm" << std::endl; */

        analysisManager->FillNtupleIColumn(0, 0, 0);
        analysisManager->FillNtupleSColumn(0, 1, "virt");
        analysisManager->FillNtupleIColumn(0, 2, track->GetTrackID());
        analysisManager->FillNtupleIColumn(0, 3, track->GetParticleDefinition()->GetPDGEncoding());
        analysisManager->FillNtupleDColumn(0, 4, track->GetKineticEnergy() / MeV);
        analysisManager->FillNtupleDColumn(0, 5, track->GetGlobalTime() / ns);
        analysisManager->FillNtupleDColumn(0, 6, mom.x()/ MeV);
        analysisManager->FillNtupleDColumn(0, 7, mom.y()/ MeV);
        analysisManager->FillNtupleDColumn(0, 8, mom.z()/ MeV);
        analysisManager->FillNtupleDColumn(0, 9, pos.x()/ mm);
        analysisManager->FillNtupleDColumn(0, 10, pos.y()/ mm);
        analysisManager->FillNtupleDColumn(0, 11, pos.z()/ mm);

        analysisManager->AddNtupleRow(0);

        /* track->SetTrackStatus(G4TrackStatus::fStopAndKill); */
    }
    return true;
}
