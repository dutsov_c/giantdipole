#include "RunAction.hh"
#include "G4ios.hh"

#include "G4InterpolationManager.hh"
#include "G4AnalysisManager.hh"
#include "Randomize.hh"

#include <filesystem>
#include <regex>

namespace fs = std::filesystem;

RunAction::RunAction() : m_messenger(new RunActionMessenger(this)) {
    G4AccumulableManager* accummulableManager = G4AccumulableManager::Instance();
    accummulableManager->RegisterAccumulable(fStoredEvents);
    accummulableManager->RegisterAccumulable(fStoppedEvents);
    accummulableManager->RegisterAccumulable(fStoppedParticles);
}

RunAction::~RunAction() {}

void RunAction::BeginOfRunAction([[maybe_unused]] const G4Run* run) {
    G4AccumulableManager* accummulableManager = G4AccumulableManager::Instance();
    accummulableManager->Reset();
    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

    if (isMaster) {
        std::string outputFolderPath = G4String("data_output/") + "job_" + runID  + "/";
        fs::path subdir (outputFolderPath);
        if(fs::create_directory(subdir)) {
            G4cout << "Directory Created: "<< fDetOutputFolder << G4endl;
        } else {
            G4cout << "Cannot create directory: "<< fDetOutputFolder << G4endl;
        }

        analysisManager->SetNtupleMerging(true);
        analysisManager->SetVerboseLevel(1);
        analysisManager->SetFileName(outputFolderPath + "DetectorOutput.root");
        analysisManager->OpenFile();

        //choose the Random engine

        if (runSeed == 0) {
            runSeed = std::stoi(std::regex_replace(runID, std::regex(R"([\D])"), ""));
        }
        G4cout << "+++++++++++++++ Random Seed +++++++++++++++++++++" << G4endl;
        G4cout << "Seed is: " << runSeed << " for run " << runID << G4endl;
        G4cout << "+++++++++++++++ Random Seed +++++++++++++++++++++" << G4endl;
        G4Random::setTheEngine(new CLHEP::HepJamesRandom);
        CLHEP::HepRandom::setTheSeed(runSeed);
        G4Random::setTheSeed(runSeed);
    }

    analysisManager->CreateNtuple("DetectorOutput", "DetectorOutputS");
    analysisManager->CreateNtupleIColumn("EventID");
    analysisManager->CreateNtupleSColumn("DetName");
    analysisManager->CreateNtupleIColumn("TrackID");
    analysisManager->CreateNtupleIColumn("ParticleID");
    analysisManager->CreateNtupleDColumn("Ekin");
    analysisManager->CreateNtupleDColumn("Time");
    analysisManager->CreateNtupleDColumn("mom_x");
    analysisManager->CreateNtupleDColumn("mom_y");
    analysisManager->CreateNtupleDColumn("mom_z");
    analysisManager->CreateNtupleDColumn("pos_x");
    analysisManager->CreateNtupleDColumn("pos_y");
    analysisManager->CreateNtupleDColumn("pos_z");
    analysisManager->FinishNtuple();

}

void RunAction::EndOfRunAction(const G4Run* run) {
    G4AccumulableManager* accummulableManager = G4AccumulableManager::Instance();
    accummulableManager->Merge();

    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
    analysisManager->Write();
    analysisManager->CloseFile();

    if (IsMaster()) {
        G4cout << "--------------------End of Global Run-----------------------" << G4endl;
        G4cout << "Stored events: " << fStoredEvents.GetValue() << G4endl;
        G4cout << "Stopped events: " << fStoppedEvents.GetValue() << G4endl;
        G4cout << "Total events: " << run->GetNumberOfEvent() << G4endl;
        G4cout << "------------------------------------------------------------" << G4endl;
        G4cout << "--------------------stopped particles-----------------------" <<G4endl;
        G4cout << "Number: " << fStoppedParticles.GetValue() << G4endl;
        G4cout << "------------------------------------------------------------" << G4endl;

    } else {
        /* PrintEndRun(); */
    }
}
