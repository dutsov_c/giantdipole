#include "RunActionMessenger.hh"
#include "RunAction.hh"

#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWith3VectorAndUnit.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIdirectory.hh"

RunActionMessenger::RunActionMessenger(RunAction* runAction) : runAction(runAction) {
    runIdCmd = new G4UIcmdWithAString("/run_params/setJobID", this);
    runIdCmd->SetGuidance("Set Job ID for folder name");
    runIdCmd->AvailableForStates(G4State_PreInit);

    runRandomSeed = new G4UIcmdWithAnInteger("/run_params/setRunSeed", this);
    runRandomSeed->SetGuidance("Set random seed for run");
}

RunActionMessenger::~RunActionMessenger() {
    delete runIdCmd;
}

void RunActionMessenger::SetNewValue(G4UIcommand* command, G4String newValue) {
    if (command == runIdCmd) {
        runAction->SetRunID(newValue);
    } else if (command == runRandomSeed) {
        runAction->SetRunSeed(std::atoi(newValue));
    }
}
