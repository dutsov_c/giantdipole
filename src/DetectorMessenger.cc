#include "DetectorMessenger.hh"
#include "DetectorConstruction.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWith3VectorAndUnit.hh"
#include "G4UIdirectory.hh"

DetectorMessenger::DetectorMessenger(DetectorConstruction* DetCons) : detectorConstruction(DetCons) {
    materialCmd = new G4UIcmdWithAString("/detector/setMaterial", this);
    materialCmd->SetGuidance("Set the material of the box.");
    materialCmd->AvailableForStates(G4State_PreInit, G4State_Idle);

    sizeCmd = new G4UIcmdWith3VectorAndUnit("/detector/setSize", this);
    sizeCmd->SetGuidance("Set the size of the box.");
    sizeCmd->SetUnitCategory("Length");
    sizeCmd->AvailableForStates(G4State_PreInit, G4State_Idle);

    positionCmd = new G4UIcmdWith3VectorAndUnit("/detector/setPosition", this);
    positionCmd->SetGuidance("Set the position of the box.");
    positionCmd->SetUnitCategory("Length");
    positionCmd->AvailableForStates(G4State_PreInit, G4State_Idle);

    runIdCmd = new G4UIcmdWithAString("/detector/setJobID", this);
    runIdCmd->SetGuidance("Set Job ID for folder name");
    runIdCmd->AvailableForStates(G4State_PreInit);
}

DetectorMessenger::~DetectorMessenger() {
    delete materialCmd;
    delete sizeCmd;
    delete positionCmd;
    delete runIdCmd;
}

void DetectorMessenger::SetNewValue(G4UIcommand* command, G4String newValue) {
    if (command == materialCmd) {
        detectorConstruction->SetMaterial(newValue);
    } else if (command == sizeCmd) {
        detectorConstruction->SetSize(sizeCmd->GetNew3VectorValue(newValue));
    } else if (command == positionCmd) {
        detectorConstruction->SetPosition(positionCmd->GetNew3VectorValue(newValue));
    }
}
