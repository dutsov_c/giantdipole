#include "DetectorConstruction.hh"
#include "G4SubtractionSolid.hh"
#include "G4Material.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4NistManager.hh"
#include "G4SystemOfUnits.hh"
#include "G4VisAttributes.hh"
#include "G4SDManager.hh"
#include "NeutronSD.hh"
#include "DetectorMessenger.hh"

DetectorConstruction::DetectorConstruction()
    : m_materialName("G4_WATER"), // Default material
      m_size(G4ThreeVector(10*cm, 10*cm, 10*cm)), // Default size
      m_position(G4ThreeVector(0, 0, 0)), // Default position
      m_messenger(new DetectorMessenger(this)) {}

DetectorConstruction::~DetectorConstruction() {
    delete m_messenger;
}

G4VPhysicalVolume* DetectorConstruction::Construct() {
    G4NistManager* nist = G4NistManager::Instance();
    G4Material* material = nist->FindOrBuildMaterial(m_materialName);

    // World
    G4double world_sizeXY = 1*m;
    G4double world_sizeZ = 1*m;
    G4Material* world_mat = nist->FindOrBuildMaterial("G4_AIR");
    G4Box* solidWorld = new G4Box("World", 0.5*world_sizeXY, 0.5*world_sizeXY, 0.5*world_sizeZ);
    G4LogicalVolume* logicWorld = new G4LogicalVolume(solidWorld, world_mat, "World");
    G4VPhysicalVolume* physWorld = new G4PVPlacement(0, G4ThreeVector(), logicWorld, "World", 0, false, 0, true);

    // Virtual detector
    G4Material* virtual_mat = nist->FindOrBuildMaterial("G4_Galactic");
    G4double padding = 10*cm;
    G4ThreeVector outerSize = m_size + G4ThreeVector(padding*2, padding*2, padding*2);
    G4ThreeVector innerSize = m_size + G4ThreeVector(padding, padding, padding);
    G4Box* solidOuterVirtualDetector = new G4Box("OuterVirtualDetector", outerSize.x()/2, outerSize.y()/2, outerSize.z()/2);
    G4Box* solidInnerVirtualDetector = new G4Box("InnerVirtualDetector", innerSize.x()/2, innerSize.y()/2, innerSize.z()/2);
    G4SubtractionSolid* solidVirtualDetectorShell = new G4SubtractionSolid("VirtualDetectorShell", solidOuterVirtualDetector, solidInnerVirtualDetector, 0, G4ThreeVector(0, 0, 0));
    G4LogicalVolume* logicVirtualDetectorShell = new G4LogicalVolume(solidVirtualDetectorShell, virtual_mat, "VirtualDetectorShell");
    new G4PVPlacement(0, m_position, logicVirtualDetectorShell, "VirtualDetectorShell", logicWorld, false, 0, true);

    G4VisAttributes* va= new G4VisAttributes(G4Color(0.2, 1, 1, 1));
    va->SetForceSolid(false);
    logicVirtualDetectorShell->SetVisAttributes(va);

    // Make the virtual detector a sensitive detector
    G4SDManager* SDman = G4SDManager::GetSDMpointer();
    NeutronSD* neutronSD = new NeutronSD("NeutronSD");
    SDman->AddNewDetector(neutronSD);
    logicVirtualDetectorShell->SetSensitiveDetector(neutronSD);

    // Material block using member variables for material, size, and position
    G4Box* solidBlock = new G4Box("Block", m_size.x()/2, m_size.y()/2, m_size.z()/2);
    G4LogicalVolume* logicBlock = new G4LogicalVolume(solidBlock, material, "Block");
    new G4PVPlacement(0, m_position, logicBlock, "Block", logicWorld, false, 0, true);

    va = new G4VisAttributes(G4Color(0.7, 0.2, 0, 0.6));
    va->SetForceSolid(true);
    logicBlock->SetVisAttributes(va);

    return physWorld;
}

void DetectorConstruction::SetMaterial(const G4String& materialName) {
    m_materialName = materialName;
}

void DetectorConstruction::SetSize(const G4ThreeVector& size) {
    m_size = size;
}

void DetectorConstruction::SetPosition(const G4ThreeVector& position) {
    m_position = position;
}
