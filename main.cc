#include "DetectorConstruction.hh"
#include "PrimaryGeneratorAction.hh"
#include "RunAction.hh"

#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "G4VisExecutive.hh"
#include "G4UIExecutive.hh"

// Physics lists
#include "FTFP_BERT_HP.hh" // Assuming FTFP_BERT_HP, but adjust according to your needs
#include "QGSP_BIC_HP.hh" // Import the QGSP_BIC_HP physics list

int main(int argc,char** argv) {
    auto runManager = new G4RunManager;

    runManager->SetUserInitialization(new DetectorConstruction());
    runManager->SetUserInitialization(new QGSP_BIC_HP); // Set the physics list here
    runManager->SetUserAction(new RunAction);
    runManager->SetUserAction(new PrimaryGeneratorAction());

    auto visManager = new G4VisExecutive;
    visManager->Initialize();

    auto UImanager = G4UImanager::GetUIpointer();
    if (argc != 1) {
        G4String command = "/control/execute ";
        G4String fileName = argv[1];
        UImanager->ApplyCommand(command+fileName);
    } else {
        auto ui = new G4UIExecutive(argc, argv);
        UImanager->ApplyCommand("/control/execute init_vis.mac");
        ui->SessionStart();
        delete ui;
    }

    delete visManager;
    delete runManager;
    return 0;
}
