#!/bin/bash

# Base directory for your Geant4 build and macro files
BUILD_DIR="./build"
MACRO_TEMPLATE="macro.mac" # Template macro file path
ENERGY_START=1
ENERGY_END=25
ENERGY_STEP=1
TARGET="Cu"

# Loop over the energy range
for energy in $(seq -f "%04g" $ENERGY_START $ENERGY_STEP $ENERGY_END); do
	# Create a temporary macro file
	TEMP_MACRO="temp_${energy}MeV_macro.mac"
	JOB_ID="$TARGET""_beam_E_${energy}"

	# Copy the template to a new temp file
	cp "$MACRO_TEMPLATE" "$TEMP_MACRO"

	# Replace energy and jobID in the temp macro file
	sed -i "s#/gps/energy .*#/gps/energy ${energy} MeV#g" "$TEMP_MACRO"
	sed -i "s#/run_params/setJobID .*#/run_params/setJobID $JOB_ID#g" "$TEMP_MACRO"

	# Execute the Geant4 application with the temporary macro file
	$BUILD_DIR/giantdipole $TEMP_MACRO

	# Optional: remove the temp macro file after running to keep the directory clean
	rm "$TEMP_MACRO"
done

echo "Simulation complete."
