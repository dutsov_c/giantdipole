import numpy as np
import matplotlib.pyplot as plt
import sys
import ROOT


def read_root_file(filename):
    # Open the ROOT file
    f = ROOT.TFile.Open(filename, "READ")
    if not f:
        print(f"Failed to open file {filename}")
        sys.exit(1)

    # Access the TTree named "DetectorOutput" (change as needed)
    tree = f.Get("DetectorOutput")
    if not tree:
        print("TTree 'DetectorOutput' not found")
        sys.exit(1)

    # Prepare a numpy array to hold kinetic energies of neutrons
    kinetic_energies = []

    # Loop over all entries in the TTree
    for event in tree:
        # Check if the particle is a neutron based on PDG ID
        if event.GetBranch("ParticleID").GetLeaf("ParticleID").GetValue() == 2112:
            # Append the kinetic energy to the list
            kinetic_energies.append(event.GetBranch("Ekin").GetLeaf("Ekin").GetValue())

    # Close the ROOT file
    f.Close()

    return np.array(kinetic_energies)


def plot_kinetic_energies(energies):
    # Plot histogram
    plt.hist(energies, bins=200, color="blue", alpha=0.7)
    plt.xlabel("Kinetic Energy (MeV)")
    plt.ylabel("Count")
    plt.title(f"Kinetic Energy of Neutrons (N={len(energies)})")
    # plt.yscale("log")
    plt.show()


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: python script.py <ROOT file path>")
        sys.exit(1)

    filename = sys.argv[1]
    energies = read_root_file(filename)
    print(len(energies))
    plot_kinetic_energies(energies)
