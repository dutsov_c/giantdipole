#ifndef NeutronSD_h
#define NeutronSD_h 1

#include "G4VSensitiveDetector.hh"
#include "G4ThreeVector.hh"
#include <iostream>

class NeutronSD : public G4VSensitiveDetector {
public:
    NeutronSD(const G4String& name);
    virtual ~NeutronSD();

    virtual G4bool ProcessHits(G4Step* aStep, G4TouchableHistory* ROhist) override;
};

#endif

