#ifndef DetectorMessenger_h
#define DetectorMessenger_h 1

#include "G4UImessenger.hh"
#include "globals.hh"

class G4UIcmdWithAString;
class G4UIcmdWith3VectorAndUnit;
class DetectorConstruction;

class DetectorMessenger : public G4UImessenger {
public:
    DetectorMessenger(DetectorConstruction* DetCons);
    ~DetectorMessenger();

    virtual void SetNewValue(G4UIcommand* command, G4String newValue);

private:
    DetectorConstruction* detectorConstruction;
    G4UIcmdWithAString* materialCmd;
    G4UIcmdWith3VectorAndUnit* sizeCmd;
    G4UIcmdWith3VectorAndUnit* positionCmd;
    G4UIcmdWithAString* runIdCmd;
};

#endif
