#ifndef RunActionMessenger_h
#define RunActionMessenger_h 1

#include "G4UImessenger.hh"
#include "G4UIcommand.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "globals.hh"

class G4UIcmdWithAString;
class G4UIcmdWith3VectorAndUnit;
class RunAction;

class RunActionMessenger: public G4UImessenger {
public:
    RunActionMessenger(RunAction* runAction);
    ~RunActionMessenger();

    virtual void SetNewValue(G4UIcommand* command, G4String newValue);

private:
    RunAction* runAction;
    G4UIcmdWithAString* runIdCmd;
    G4UIcmdWithAnInteger* runRandomSeed;
};

#endif
