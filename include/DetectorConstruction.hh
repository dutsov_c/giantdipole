#ifndef DetectorConstruction_h
#define DetectorConstruction_h 1

#include "G4VUserDetectorConstruction.hh"
#include "G4ThreeVector.hh"
#include "globals.hh"

class DetectorMessenger; // Forward declaration

class DetectorConstruction : public G4VUserDetectorConstruction {
public:
    DetectorConstruction();
    virtual ~DetectorConstruction();
    virtual G4VPhysicalVolume* Construct();
    
    void SetMaterial(const G4String& materialName);
    void SetSize(const G4ThreeVector& size);
    void SetPosition(const G4ThreeVector& position);

private:
    G4String m_materialName;
    G4ThreeVector m_size;
    G4ThreeVector m_position;
    DetectorMessenger* m_messenger;
};

#endif

