#ifndef RunAction_h
#define RunAction_h 1

#include "DetectorConstruction.hh"
#include "RunActionMessenger.hh"

#include "G4UserRunAction.hh"
#include "G4AccumulableManager.hh"
#include "G4Run.hh"

class G4Run;
class RunActionMessenger;

class RunAction : public G4UserRunAction
{
public:

    explicit RunAction();
    ~RunAction() override;

    void BeginOfRunAction(const G4Run*) override;
    void EndOfRunAction(const G4Run*) override;

    G4String fDetOutputFolder = "";

    G4Accumulable<G4int> fStoredEvents = 0;
    G4Accumulable<G4int> fStoppedEvents = 0;
    G4Accumulable<G4int> fStoppedParticles = 0;

    void SetRunID(G4String val) {
        runID = val;
    };

    void SetRunSeed(G4int val) {
        runSeed = val;
    }

    RunActionMessenger* m_messenger;

private:
    G4String runID = "0";
    G4int runSeed = 0;

};
#endif
